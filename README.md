# PrivacyTools.io OpenSearch

[![License](https://img.shields.io/badge/license-GPLv3-ff69b4.svg)](https://gitlab.com/nitrohorse/search-privacytools-io/blob/master/LICENSE)
[![Downloads](https://img.shields.io/amo/users/search-privacytools-io.svg)](https://addons.mozilla.org/en-US/firefox/addon/search-privacytools-io)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

[OpenSearch XML](https://developer.mozilla.org/en-US/docs/Web/OpenSearch) that adds [search.privacytools.io](https://search.privacytools.io) as a search engine to the Firefox browser. Submits the query via POST request as this is [Searx's default](https://search.privacytools.io/about):

> Queries are made using a POST request on every browser (except chrome*). Therefore they show up in neither our logs, nor your url history.

## Download
* https://addons.mozilla.org/en-US/firefox/addon/search-privacytools-io
